<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
// use Illuminate\Foundation\Testing\TestCase;
use Tests\TestCase;
use App\Models\Companies;
use App\Models\Employees;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AuthTest extends TestCase
{
    /**
     * Employee login to their company site.
     *
     * @return void
     */

    public function logged_in_employee()
    {
        $domain = 'companytest';

        $url = "http://" . $domain . ".localhost:8000";
        $current_url = explode('.', parse_url($url, PHP_URL_HOST));

        $company = Companies::factory()->create([
            'domain' => $domain,
        ]);
        $employee = Employees::factory()->create([
            'company' => $company->id,
            'password' => Hash::make('employee'),
        ]);

        $this->actingAs($employee)->withSession([
            'token'             =>  $employee,
            'current_company'   =>  $current_url[0],
            'company_employee'  =>  $company->domain,
            'company_id'        =>  $company->id
        ]);
    }

    /**
     * Employee can view employees list of their company after login.
     *
     * @return void
     */
    public function test_employee_can_view_employees_list_after_logged_in()
    {
        $this->logged_in_employee();
        $list_employees_url = "http://" . Session::get('current_company'). "." . "localhost:8000/home";

        $response = $this->get($list_employees_url);
        $response->assertOk();
    }

}
