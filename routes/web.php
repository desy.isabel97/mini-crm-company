<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmployeesController;
use App\Http\Controllers\Auth\Login\EmployeeAuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false, 'reset' => false]);
Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'App\Http\Controllers\LanguageController@switchLang']);

Route::domain('{company}.' . config('app.short_url'))->group(function () {
	Route::get('/', function () {
		return view('auth/login');
	});

	Route::get('/login', function () {
		return view('auth/login');
	});   
		
});

Route::group(['middleware' => ['auth', 'checkDomain']], function () { 
	Route::get('/', [EmployeesController::class, 'index']);
	Route::get('/home', [EmployeesController::class, 'index']); 
Route::resource('employees', EmployeesController::class);
});