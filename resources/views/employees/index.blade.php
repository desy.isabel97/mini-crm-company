@extends('adminlte::page')

@section('title', 'Employees List')

@section('content_header')
    <h1>{{ trans('text.Employees List') }}</h1>
@stop

@section('content')<!doctype html>
    <hr>
    
    <br/>

    <div class="container">
        <table class="table table-bordered" id="employee_table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>{{trans('text.First Name')}}</th>
                    <th>{{ trans('text.Last Name') }}</th>
                    <th>{{ trans('text.Email') }}</th>
                    <th>{{ trans('text.Phone') }}</th>
                    <th width="25%">{{ trans('text.Action') }}</th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
                <tr>
                    <td></td>
                    <td>
                        <input type="text" data-column="1" placeholder="First Name...." class="form-control filter-input">
                    </td>
                    <td>
                        <input type="text" data-column="2" placeholder="Last Name...." class="form-control filter-input">
                    </td>
                    <td>
                        <input type="text" data-column="3" placeholder="Email...." class="form-control filter-input">
                    </td>
                    <td>
                        <input type="text" data-column="4" placeholder="Phone...." class="form-control filter-input">
                    </td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>
@stop

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function(){

            load_data();

            function load_data(timezone = ''){
                var t = $('#employee_table').DataTable({
                    processing: true,
                    serverSide: true,
                    bLengthChange: false,
                    ajax: {
                        url: "{{ route('employees.index') }}",
                    },
                    columns: [
                        // {
                        //     data: 'id',
                        //     name: 'id'
                        // },
                        {data: 'rownum', name: 'rownum'},
                        {
                            data: 'first_name',
                            name: 'first_name'
                        },
                        {
                            data: 'last_name',
                            name: 'last_name'
                        },
                        {
                            data: 'email',
                            name: 'email'
                        },
                        {
                            data: 'phone',
                            name: 'phone',
                            orderable: false
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false
                        }
                    ],
                    "columnDefs": [ {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    } ],
                    "order": [[ 1, 'asc' ]]
                });

                $('.filter-input').keyup(function(){
                    t.column( $(this).data('column'))
                        .search($(this).val())
                        .draw();
                });

                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    } );
                } ).draw();
            };

            $('#filter').click(function() {
                var timezone = $('#timezone').val();
                if(timezone != ''){
                    $('#employee_table').DataTable().destroy();
                    load_data(timezone);
                };
            });  
        });
</script>
@stop