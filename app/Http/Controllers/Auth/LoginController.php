<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\Companies;
use App\Models\Employees;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request, $user)
    {
        $url = url()->current();
        $current_url = explode('.', parse_url($url, PHP_URL_HOST));

        $company_employee = Companies::where('id', '=', $user->company)->first();
        $current_company = Companies::where('domain', '=', $current_url[0])->first();

        session([
            'current_company' => $current_url[0],
            'company_employee' => $company_employee->domain,
            'company_id' => $current_company->id
        ]);

        // dd(session()->all());
    }
}
