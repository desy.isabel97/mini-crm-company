<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Companies;
use App\Models\Employees;
use DB;
use Yajra\Datatables\Datatables;

class EmployeesController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth:emplo');
    // }

    // public function index()
    // {
    //     return view('home.employee');
    // }

    public function index(Request $request)
    {
        // $request->route()->forgetParameter('subdomain');
        // $company = Companies::with('employees')
        //     ->whereSubdomain($subdomain)
        //     ->firstOrFail();
        $company = \Session::get('company_id');
        
        if($request->ajax()) {
            DB::statement(DB::raw('set @rownum=0'));
            $data = Employees::select([
                DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                'id',
                'first_name',
                'last_name',
                'email',
                'phone',
            ])->where('company', '=', $company);
            return DataTables::of($data)
                // ->editColumn('created_at', function ($value){
                //     return setNewTimezone($value->created_at);
                // })
                ->addColumn('action', function($data){
                    // $button = '<a href="/' . $data->id . '/view-employee" class="btn btn-success btn-sm">View</a>
                    // ';
                    // $button .= '<button type="button" name="edit" id="'.$data->id.'" class="edit btn btn-primary btn-sm">Edit</button>';
                    // $button .= '&nbsp;<button type="button" name="edit" id="'.$data->id.'" class="delete btn btn-danger btn-sm">Delete</button>';
                    // return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        // $companies = Companies::pluck('name', 'id');

        // return view('employees-index', [
        //     'companies' => $companies,
        //     // 'timezones' => $timezones,
        // ]);
        
        return view('employees.index'); //
    }

    // public function show($subdomain, Product $product)
    // {
    //     $product->load('created_by');

    //     return view('products.show', compact('product'));
    // }
}
