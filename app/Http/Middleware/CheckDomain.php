<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckDomain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // dd('session()->all()');
        if (\Session::get('current_company') !== \Session::get('company_employee')) {
            Auth::logout();
            $request->session()->flush();
            return redirect()->to('/login');

        }
        else{
            return $next($request);
        }
        // return $next($request);
    }
}
